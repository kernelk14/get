#!/usr/bin/env python3

import os
import getopt
import sys

def usage():
    print("""Usage: ./get.py [flags] <creator/reponame>
Example: ./get.py -L kernelk14/get
Get: it gets a certain repo.

Help:
        -L     --gitlab             Get the repo from Gitlab
        -H     --github             Get the repo from Github
        -h     --help               Print this help and exit
    """)
# TODO: Find a way to handle arguments
argv = sys.argv
argList = argv[1:]
opts = "LH:h"
long_opts = ["gitlab", "github", "help"]
# reponame = argv[2]
try:
    if len(sys.argv) < 2:
        print("Error: No arguments given\n")
        usage()
        exit(1)
    args, vals = getopt.getopt(argList, opts, long_opts)
    for currentArgument, currentValue in args:
        if currentArgument in ('-L', '--gitlab'):
            try:
                os.system(f"git clone https://gitlab.com/{argv[2]}")
                exit(0)
            except IndexError:
                print("Error: no reponame given")
                exit(1)
        if currentArgument in ('-H', '--github'):
            try:
                os.system(f"git clone https://github.com/{argv[2]}")
                exit(0)
            except IndexError:
                print("Error, no reponame given")
                exit(1)
        if currentArgument in ('-h', '--help'):
            print("Printing as help")
            usage()
            exit(0)
except getopt.error as err:
    print(str(err))
    # print("Printing as a help function")
usage()


